import com.mongodb.MongoClient;
import com.mongodb.client.DistinctIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.Set;
import java.util.TreeSet;
import java.util.function.Consumer;

public class MongoDBImagesDelete {
    public static void main(String[] args) {
        MongoClient client = new MongoClient("localhost", 27017);
        MongoDatabase mongoDatabase = client.getDatabase("photo-sharing");
        MongoCollection<Document> images = mongoDatabase.getCollection("images");
        MongoCollection<Document> albums = mongoDatabase.getCollection("albums");
        Set<Integer> imagesInAlbumsIDs = new TreeSet<>();
        albums.distinct("images", Integer.class).forEach((Consumer<? super Integer>) imagesInAlbumsIDs::add);
        FindIterable<Document> imagesDocuments = images.find();
        for (Document img: imagesDocuments) {
            if (imagesInAlbumsIDs.contains(img.getInteger("_id")));
            else images.deleteOne(img);
        }
        System.out.println("In images collection: "+images.count());
        System.out.println("Images with the tag \"sunrises\": "+images.count(new Document("tags","sunrises")));
    }
    }
