import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;

public class MongoDBGradesDelete {
    public static void main(String[] args) {
        MongoClient client = new MongoClient("localhost", 27017);
        MongoDatabase mongoDatabase = client.getDatabase("students");
        MongoCollection<Document> collection = mongoDatabase.getCollection("grades");

        Bson sort = new Document("student_id",1).append("score",1);
        List<Document> docs = collection
                .find(Filters.eq("type", "homework"))
                .sort(sort)
                .into(new ArrayList<>());

        for (int i = 0; i < docs.size(); i++) {
            if (i % 2 == 0)
                System.out.println(docs.get(i));
                /*collection.deleteOne(docs.get(i));*/
        }

        System.out.println(collection.count());

    }


}
