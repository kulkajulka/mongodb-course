import com.mongodb.GroupCommand;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Projections.*;
import static java.util.Arrays.asList;

public class MongoDBStudentsUpdate {
    public static void main(String[] args) {
        MongoClient client = new MongoClient("localhost", 27017);
        MongoDatabase mongoDatabase = client.getDatabase("school");
        MongoCollection<Document> collection = mongoDatabase.getCollection("students");

        Bson sort = new Document("_id",1).append("scores.score",1);
        Bson projection = Filters.eq("scores.type","homework");
        List<Document> docs = collection
                .aggregate(asList(new Document("$unwind","$scores"), new Document("$match",new Document("scores.type","homework")),
                        new Document("$sort",new Document("_id",1).append("scores.score",1)))).into(new ArrayList<>());

        for (int i = 0; i < docs.size(); i++) {
            if (i % 2 == 0) {
                Document temp = docs.get(i);
                System.out.println(temp);
                System.out.println(temp.get("_id"));
                System.out.println(temp.get("scores",Document.class).get("score"));
                System.out.println(temp.get("scores",Document.class).get("score") instanceof Double);
                collection.updateOne(new Document("_id", temp.getInteger("_id")),
                        new Document("$pull",new Document("scores",
                                new Document("type","homework").append("score",temp.get("scores",Document.class).get("score")))));
            }
        }

    }
}
